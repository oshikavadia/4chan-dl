package chan4;

import org.json.*;
import java.net.*;
import java.io.*;
import java.util.regex.*;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Oshi
 */
public class Chan_DL {

    public static void main(String[] args) throws MalformedURLException, IOException {
        System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");
        String url = "", board = "";
        if (args.length == 1) {
            url = args[0] + ".json";
        } else {
            System.out.println("No arguements found...");
            System.exit(1);
        }

        board = getBoard(url);

        URL URL = new URL(url);

        String rawJSON = getRawJSON(URL);

        JSONObject obj = getJSON(rawJSON);

        JSONArray arr = obj.getJSONArray("posts");
        String folderName = "";
        int imageCount = 0,currentImage = 0;
        for (int i = 0; i < arr.length(); i++) {
            JSONObject jObject = arr.getJSONObject(i);
            
            if(jObject.has("semantic_url")){
                new File(jObject.getString("semantic_url")).mkdir();
                folderName = jObject.getString("semantic_url").replaceAll("[^a-zA-Z0-9.-]", "_");
                
//                System.setProperty("user.dir", folderName);
            }
            
            if (jObject.has("images")) {
                imageCount = jObject.getInt("images") + 1;
                System.out.println("Downloading " + imageCount + " images...");
            }
            if (jObject.has("filename") && jObject.has("tim") && jObject.has("ext")) {
                currentImage++;
                String filename = jObject.getString("filename");
                long tim = jObject.getLong("tim");
                String ext = jObject.getString("ext");
                System.out.print("("+currentImage+"/"+imageCount+")");
                saveImage(folderName,filename, tim, ext, board);
            }
        }
    }

    public static String getRawJSON(URL URL) throws IOException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(URL.openStream())
        );

        String inputLine;
        String output = "";
        while ((inputLine = in.readLine()) != null) {
            output += inputLine;
        }
        in.close();

        return output;
    }

    public static String getBoard(String url) {
         Pattern p = Pattern.compile("\\/[a-z]{1,}\\/");
         Matcher m = p.matcher(url);
         if (m.find()) {
         //            System.out.println(m.group(0));
         return m.group(0).replaceAll("/", "");
         } else {
         System.out.println("Board not found...");
         System.exit(1);
         return "";
         }
         
        
    }

    public static JSONObject getJSON(String rawJson) {
        return new JSONObject(rawJson);
    }

    public static void saveImage(String folder , String filename, long tim, String ext, String board) throws IOException {
        
        System.out.println("Saving " + filename + ext);
        URL imageURL = new URL("http://i.4cdn.org/" + board + "/" + tim + ext);
        
        FileUtils.copyURLToFile(imageURL, new File(folder+"\\"+filename + ext));
    }
}
